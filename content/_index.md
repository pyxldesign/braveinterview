---
title: "Basic Attention Token"
subtitle: "Introducing blockchain based digital advertising"
date: "July 1, 2021"
description: "From the creator of JavaScript and the co-founder of Mozilla and Firefox, with a solid team – funded by Founders Fund, Foundation Capital, Propel Venture Partners, Pantera Capital, DCG, Danhua Capital, and Huiyin Blockchain Venture among others."
draft: false

# Home Hero Buttons
heroButtons:
   - button:
      type: "modal"
      id: "home-hero--modal-btn"
      controls: "#home-hero--modal"
      ariaLabel: "Opens light box to show Brave Video."
      icon: "icons/play-filled.svg"
      text: "Video"
      iframe: "<iframe title=\"vimeo-player\" loading=\"lazy\" src=\"https://player.vimeo.com/video/533695950?dnt=true\" width=\"640\" height=\"360\" frameborder=\"0\" allowfullscreen></iframe>"
   - button: 
      type: "link"
      rel: "nofollow"
      ariaLabel: "Get brave - External link to brave.com"
      icon: "icons/brave-logo.svg"
      text: "Get Brave"
      link: "https://brave.com/"

# Upper Content
upperContent:
   - card:
      title: "Open Source. Transparent. Decentralized. Efficient."
      links:
         - link:
            icon: "icons/icon-article.svg"
            text: "View the White Paper"
            link: "#"
         - link:
            icon: "icons/icon-article.svg"
            text: "On Token Velocity"
            link: "#"
   - card:
      icon: "icons/shield.svg"
      content: "Basic Attention Token radically improves the efficiency of digital advertising by creating a new token that can be exchanged between publishers, advertisers, and users. It all happens on the Ethereum blockchain."
   - card:
      icon: "icons/velocity.svg"
      content: "The token can be used to obtain a variety of advertising and attention-based services on the BAT platform. The utility of the token is based on user attention, which simply means a person’s focused mental engagement."

# The Challenge
challenge:
   title: "The Challenge"
   subtitle: "Digital advertising is overrun by middlemen, trackers and fraud."
   cards:
      - card:
         icon: "icons/target.svg"
         title: "Users are abused"
         list:
            - "Up to 50% of the average user’s mobile data is for ads and trackers, costing as much as $23 a month."
            - "Ads use about 5 seconds of mobile load time on average."
            - "Ads decrease phone battery life by as much as 21%."
            - "Privacy is violated when large media sites host up to 70 trackers."
            - "Malware (malvertisements, ransom-ware) is up 132% in one year."
      - card:
         icon: "icons/bot.svg"
         title: "Publishers are hurting"
         list:
            - "Google and Facebook take 73% of all ad dollars and 99% of all growth."
            - "Revenue is recently down 66%."
            - "Bots inflicted $7.2 billion in fraud last year."
            - "Over 600 million phones and desktops run ad-blocking."
            - "Publishers cannot seamlessly monetize value added services. "
      - card:
         icon: "icons/info.svg"
         title: "Advertisers are losing"
         list:
            - "Advertisers lack good information on what they are paying for."
            - "Marketers are often fooled by bogus websites and bots that commit fraud."
            - "Targeting is poor, making users more likely to ignore ads.  "

# Split Content
splitContent:
   image: "images/brave-plus-bat.png"
   title: "Blockchain Digital Advertising"
   subtitle: "Introducing a decentralized, transparent digital ad exchange based on Ethereum Blockchain."
   steps: 
      - step: 
         icon: "icons/brave.svg"
         title: "Stage 1: Brave Browser"
         content: "<p>Brave is a fast, open source, privacy-focused browser that blocks malvertisements, trackers, and contains a ledger system that anonymously captures user attention to accurately reward publishers.</p>"
      - step: 
         icon: "icons/bat.svg"
         title: "Stage 2: Basic Attention Token"
         content: "<p>The Basic Attention Token can be used to obtain a variety of advertising and attention-based services on the BAT platform, as it is exchanged between publishers, advertisers, and users.</p><p>The token’s utility is derived from — or denominated by — user attention.</p><p>Attention is really just focused mental engagement — on an advertisement, in this case.</p>"
      - step: 
         icon: "icons/velocity.svg"
         title: "Stages 1 + 2 = A New Deal"
         content: "<p>The Brave browser knows where users spend their time, making it the perfect tool to calculate and reward publishers with BATs. This service creates a transparent and efficient Blockchain-based digital advertising market. Publishers receive more revenue because middlemen and fraud are reduced. Users opt-in to an inclusive and rewarding private ad experience. And advertisers get better data on their spending.</p>"
   columnImage: "images/browser-graphic.jpeg"

# Works
works:
   title: "How It Works"
   description: "The Brave browser anonymously monitors user attention, then rewards publishers accordingly with BATs."
   backgroundImage: "images/works-background.png"
   cards: 
      - card:
         title: "Measuring Attention"
         content: "<p>Attention is measured as users view ads and content in the browser’s active tab in real time. The Attention Value for the ad will be calculated based on incremental duration and pixels in view in proportion to relevant content, prior to any direct engagement with the ad. We will define further anonymous cost-per-action models as the system develops.</p><p>Ads are then anonymously matched with customer interests using local machine learning algorithms. This means fewer irrelevant ads.</p><p>Brave will work with publishers and advertisers to establish best practices for judging user attention. One potential metric: the number of total views of advertising content for a certain number of seconds. Or, points assigned on the length of a view.</p>"
      - card:
         title: "Who Gets What?"
         content: "<p>Users viewing ads will be rewarded with BATs. BATs can be used for premium content or services on the BAT platform.</p><p>Publishers will, as part of this service, receive the lion’s share of the total ad revenue. We anticipate that users will also donate back some tokens to publishers, further increasing their revenue.</p>"
      - card:
         title: "More Privacy, Less Fraud"
         content: "<p>We plan to mitigate possible ad fraud through the use of cryptography, better client-side integrity, and transparency achieved through open source.</p>"
      - card:
         title: "Up and Running"
         content: "<p>Much of the infrastructure required to deploy BAT at the back end is up and running, meaning it’s currently in place and being used to distribute donations to publishers based on customer attention. Use it today in the Brave Browser for Windows, Mac OS and Linux.</p>"

---
