
(function($) {
   const pyxlScripts = {
      Init : function() {
         const _this = this;

         if($('#home-hero--modal-btn').length) {
            this.Modal();
         }

      },
      Modal : function() {
         $("#home-hero--modal-btn").on("click", function() {
            if($(this).hasClass('open')) {
               $('#home-hero--modal').stop().fadeOut().attr('aria-hidden', 'true');
               $(this).removeClass('open').attr('aria-expanded', 'false');
            } else {
               $('#home-hero--modal').stop().fadeIn().attr('aria-hidden', 'false');
               $(this).addClass('open').attr('aria-expanded', 'true');
            }
         })
         $('#close-home-hero--modal').on("click", function() {
            $("#home-hero--modal-btn").click();
         })
      }
   }

   pyxlScripts.Init();

})(jQuery)